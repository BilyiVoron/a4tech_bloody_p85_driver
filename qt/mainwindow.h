#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private slots:
//	void on_findButton_clicked();
	void on_listWidget_doubleClicked(const QModelIndex &index);
	void on_radioButtonRR125_clicked(bool checked);
	void on_radioButtonRR250_clicked(bool checked);
	void on_radioButtonRR500_clicked(bool checked);
	void on_radioButtonRR1000_clicked(bool checked);
	void on_radioButtonLEDBrightness0_clicked(bool checked);
	void on_radioButtonLEDBrightness1_clicked(bool checked);
	void on_radioButtonLEDBrightness2_clicked(bool checked);
	void on_radioButtonLEDBrightness3_clicked(bool checked);
	void on_radioButtonCalibrationPreset_clicked(bool checked);
	void on_radioButtonCalibrationCalibration_clicked(bool checked);
	void on_comboBoxCalibration_currentIndexChanged(int index);
	void on_pushButtonRefreshDevices_clicked();
	void on_pushButtonRestartFW_clicked();
	void on_pushButtonGetRNG_clicked();
	void on_pushButtonDumpMemory_clicked();
	void on_spinBox_valueChanged(int arg1);
	void on_pushButtonDPIMode1_clicked();
	void on_pushButtonDPIMode2_clicked();
	void on_pushButtonDPIMode3_clicked();
	void on_pushButtonDPIMode4_clicked();
	void on_pushButtonDPIMode5_clicked();
	void on_pushButtonExecuteCodes_clicked();
	void on_comboBoxDPISens_currentIndexChanged(int index);

private:
	Ui::MainWindow *ui;
	void loadTextFile();
	void initMouse();
	void initMouseNew();
};

#endif // MAINWINDOW_H

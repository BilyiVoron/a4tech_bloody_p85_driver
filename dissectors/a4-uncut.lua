local ashex = Struct.tohex
local a4 = Proto("usb-column-hack", "usb_dummy_proto")
local f_usb_data_fragment = Field.new("usb.data_fragment")
local f_usb_control_response = Field.new("usb.control.Response")
local f_usb_capdata = Field.new("usb.capdata")
function a4.dissector(tvb, pinfo, tree)
    local usb_data = f_usb_data_fragment() or f_usb_control_response() or f_usb_capdata()
    if usb_data then
        local label = ashex(usb_data.range:raw())
        -- change 0011aabbccdd into "0011 aabb ccdd "
        label = string.gsub(label, "....", "%0 ")
        pinfo.cols.info:set(label)
    end
end
register_postdissector(a4)

mkdir -p ~/.config/wireshark/plugins
# Either use the cut version that only shows the bloody packet
rm -f ~/.config/wireshark/plugins/a4-uncut.lua && cp a4-cut.lua ~/.config/wireshark/plugins/
# Or the uncut version that shows the full USB packet
rm -f ~/.config/wireshark/plugins/a4-cut.lua && cp a4-uncut.lua ~/.config/wireshark/plugins/

Dissectors made with the massive help of Lekensteyn from #wireshark on Freenode

!(usb.dst == "3.54.2") && !(usb.src == "3.54.2") && !(usb.dst == "3.54.1") && !(usb.src == "3.54.1")  && !(usb.dst == "3.54.0") && !(usb.src == "3.54.0")  && !(usb.dst == "3.4.1") && !(usb.src == "3.4.1") && !(usb.dst == "3.4.0") && !(usb.src == "3.4.0") && !(frame.len == 64) && !(frame.len == 68) && !(frame.len == 80) && !(frame.len == 86)

usb.bus_id == 1 && (usb.device_address == 12 || usb.device_address == 12) && ((frame.len == 70) || (frame.len == 128))

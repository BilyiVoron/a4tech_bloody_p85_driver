#pragma once

#include <map>
#include <libusb-1.0/libusb.h>
#include <vector>
#include <string>

using namespace std;

static const int A4TECH_VID = 0x09da;
// The PID section is largely completely useless. Mouse PID is based on HW ID, so it will be different for every single mouse unless ID is cloned.
static const int BLOODY_A9_PID = 0x1003; //Pre-2018 - ???
static const int BLOODY_V5_PID = 0x172A; //Pre-2018 - ???
static const int BLOODY_V7_PID = 0x1080; //2018.0129 - V_P3305_010
static const int BLOODY_V8_PID = 0x11F5; //Pre-2018 - ???
static const int BLOODY_V8M_PID = 0x36A1; //2018.0129 - V_P3305_010
static const int BLOODY_R7_PID = 0x1485; //Pre-2018 - ???
static const int BLOODY_R8_PID = 0x14ee; //2018.0129 - DongleM_SN8F2251B_040 // Possibly renamed to R80?
static const int BLOODY_R3_PID = 0x1a5a; //Pre-2018 - ???
static const int BLOODY_AL90_PID = 0xf633; //2019.1230 - FLc_A9800_030
static const int BLOODY_R70_PID = 0xf643; //Pre-2018 - ???
static const int BLOODY_P85_PID = 0x57f7; //Pre-2018 - ???
static const int BLOODY_P85_C3_PID = 0x14a6; //Pre-2018 - ???
static const int BLOODY_P93_PID = 0x3530; //2018.0129 - P88Rcir_P3325_218
static const int SWITCHING_PID = 0xf11b; //Core switch
//static const int SWITCHING_PID = 0xf0f0; //Core switch on older mice?

//ZL50 (Activated) - ZLcir_A9800_0F0_0_2E

static const int COMPATIBLE_PIDS[] = {BLOODY_A9_PID, BLOODY_V5_PID, BLOODY_V7_PID, BLOODY_V8_PID, BLOODY_R7_PID, BLOODY_R3_PID, BLOODY_AL90_PID, BLOODY_R70_PID, BLOODY_P85_PID, BLOODY_P85_C3_PID, BLOODY_P93_PID};
static const size_t COMPATIBLE_PIDS_SIZE = sizeof(COMPATIBLE_PIDS)/sizeof(COMPATIBLE_PIDS[0]);

static const int A4TECH_MAGIC = 0x07;

static const int BACKLIGHT_OPCODE = 0x11;
static const int BACKLIGHT_WRITE = 0x80;
static const int BACKLIGHT_READ = 0x00;


class Mouse {
public:
	~Mouse();
	void init();
	vector< vector<string> > listDevices();
	std::map<int, libusb_device_handle*> getDevices() const;
	std::string getDeviceNameById(const int & id) const;

	bool selectDevice(int address);
	int setLEDBrightness(size_t bytesNeeded, uint8_t level);
	uint8_t getLEDBrightness(size_t bytesNeeded);
	void RGBEditMode(size_t bytesNeeded, std::string toggle);
	void dumpMemory(size_t bytesNeeded);
	void restartFirmware(size_t bytesNeeded);
	void SetSensitivity(size_t bytesNeeded, uint8_t modesEnabled, uint8_t modeSelected, int currentDPI, uint8_t responseRate);
	vector<uint8_t> getRNG(size_t bytesNeeded);
	std::string getMouseID(size_t bytesNeeded);
	uint8_t getSensorCalibration(size_t bytesNeeded);
	void setSensorCalibration(size_t bytesNeeded, int presetEnabled,int calibration);
	uint8_t assembleToUint8t(uint8_t left, uint8_t right);
	uint8_t leftUint8t(uint8_t fuck);
	uint8_t rightUint8t(uint8_t fuck);
	vector<uint8_t> setMouseID(size_t bytesNeeded);
	void setLEDs(size_t bytesNeeded, uint8_t LEDS[3][8]);
	std::string getInitStatus(size_t bytesNeeded);
	vector<uint8_t> getMagicSensBytes(int firstSens, int modesEnabled, int selectedMode, int sensorCalibration, int calibrationPreset, int responseRate); // Rework to have same params as savesenstomem
	void saveSensToMemory(size_t bytesNeeded, uint8_t modesEnabled, uint8_t modeSelected, int currentDPI, uint8_t responseRate, int presetEnabled,int calibration, int DPIMode1, int DPIMode2, int DPIMode3, int DPIMode4, int DPIMode5);
	vector<uint8_t> testCode(size_t bytesNeeded, std::string code);
	void print_endpoint(const struct libusb_endpoint_descriptor *endpoint);
	void print_endpoint_comp(const struct libusb_ss_endpoint_companion_descriptor *ep_comp);
	uint8_t bitmaskAlgo(int input, int multiplier, int secondaryMultiplier, int baseValue);
	std::string hex_to_string(std::string& input);
	std::string string_to_hex(const std::string& input);
	std::string uint8_vector_to_hex_string(const vector<uint8_t>& v);
	std::string AddSeparators(const std::string & s);

private:
	std::map<int, libusb_device_handle*> devices;
	libusb_device_handle* currentDevice = nullptr;
	libusb_context* context = nullptr;

	int writeToMouse(vector<uint8_t> data, size_t size);
	int readFromMouse(vector<uint8_t> request, size_t requestSize, vector<uint8_t> * response, size_t responseSize);

	void discoverDevices();

	bool isCompatibleDevice(libusb_device_descriptor &desc);
};

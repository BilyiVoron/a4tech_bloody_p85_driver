a4tech-bloody-linux-driver
====================

Forked from https://github.com/maxmati/a4tech-bloody-linux-driver

Linux config tool for A4TECH Bloody mouse series. https://www.bloody.com/en/products.php?pid=10

Useful links:
https://www.youtube.com/watch?v=qBCelkEs8bc

I specifically own the P series only, so I'll be focusing on those first, compatibility with other series comes second.

Currently in early WIP phase, trying to reverse the protocol.

If you want to submit captures, please try name the file as follows, otherwise the captures are extremely hard to identify and make use of.
```
[Mouse model][Bloody version][Bloody tab you're recording for]; relevant data in the tab
```
Here's an example for sensitivity setting.
```
[P93][2018.1005][Sensitivity];[100];1000Hz;Calibration1preset.pcapng
```

# Install:

## Arch Linux
[AUR/a4tech-bloody-driver-git](https://aur.archlinux.org/packages/a4tech-bloody-driver-git/)

## Manual

Windows:
```
I couldn't get libusb to work properly, if you want to try, create a new C++ project
in Visual Studio 2017 and import main.cpp, Mouse.h and Mouse.cpp into it
Fix the remaining errors, make a pull request and let's celebrate.
```

Linux udev rule:
```
$ echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="09da", MODE="0666"' | sudo tee /etc/udev/rules.d/a4.rules
# Replug the mouse to reapply new udev settings
```

Linux CLI:
```
$ git clone https://gitlab.com/C0rn3j/a4tech_bloody_p85_driver/ && cd a4tech_bloody_p85_driver
$ cmake ./ && make -j$(nproc)
```

Linux GUI:
```
cd qt
qmake bloody.pro -spec linux-g++ CONFIG+=qtquickcompiler
make -j$(nproc) && cd ..
```

# Run:
```
# CLI version
./bloody-cli
# GUI version
./qt/bloody
```

![cli picture](images/2018-04-05.png)

However you most likely want to try the GUI app, atm no release, just build it yourself:

![gui picture](images/GUI.png)

You will be presented with the above menu. The only functional thing at the moment is setting backlight level and temporary LED color setting for P series. Oh and code testing, if you want to help with RE.

You could for example set backlight off by sending:

```
07110000800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
```

and then turn it back on again by sending:

```
07110000800000000300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
```

# Firmware

The mouse firmware is reflashed every time you change a core or update the software.

The firmware files can be found encrypted (decryption method of anything will not be described in this project to avoid getting shut down) in the root folder of Bloody under the FW folder.

They're marked from 0 to 3, which corresponds to core 4, 1, 2, 3 in this order.

To know which firmware your mouse uses, download procmon from sysinternals, set it to only include Bloody6.exe ProcessName and change core. The file will be in one of the generated events.

You can disassemble decrypted firmware files with the following tool - https://github.com/Marisa-Chan/sn8dis

# Dev

Files of interest are Mouse.cpp, Mouse.h and main.cpp.

data.txt has RE info about the protocol itself, although I'm trying to move the protocol documentation completely to https://a4.rys.pw

Following tcpdump command prints pcap files in a readable format(gitlab murders tabs, there is a single tab before both 0x00. You can paste it in terminal by doing CTRL+V; TAB. Just define your own filename. (Or you can snatch it off the following website in the correct format || You can open the README.md raw where the tabs are correct)
```
#!/bin/bash
# Save this script as /usr/local/sbin/a4
set -euo pipefail

if [[ -z ${1-} ]]; then
	echo "You need to run this tool with a .pcapng file as a parameter."
	echo "The file has to be in the same folder as your current pwd - `pwd`"
	echo "Example: a4 captureCalibrationIntoPreset.pcapng"
	echo "Result will be copied to your clipboard, you should paste it into https://a4.rys.pw"
	exit 1
fi
filename=$1;
echo "$filename" > "/tmp/$filename.txt";
tcpdump -r "$filename" -x >> "/tmp/$filename.txt";
sed -i s/"	0x0000:  "//g "/tmp/$filename.txt";
sed -z -i s/"\n	0x00[a-f0-9][a-f0-9]:  "/" "/g "/tmp/$filename.txt";
cat "/tmp/$filename.txt" | xclip -i -selection c
rm "/tmp/$filename.txt"
```
Then paste your clipboard into the following site - it will colorize them for better readability while also explaining the protocol - https://a4.rys.pw

# P85 capabilities

This is the documentation for the mouse, what the driver can do, if it's been reversed yet and if it's been implemented in the driver.

## Mouse ID

The mouse firmware can be licensed to allow macros, described as Core 3 and Core 4 profiles.

For that you need the license and the license is tied to Mouse ID (So have fun paying another $10 when your mouse dies).

You can get Mouse ID by executing this query:
```
07050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
```

You will get a bunch of yet-unknown data and the Mouse ID.
```
07050000000000000100e803002a002c003e5512f03f8b1200000401759f2fe1ffff094a00005affff078a5cfeff000000000000000000000000000000000000
                                                        11111111
11111111 - Reversed Mouse ID - E12F9F75
```

## DPI/Hz

### Set DPI

#### Current
- [X] Implemented

```
070d0000000000000403737300010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 # 5000Hz
                2233!!!!  11
33 - mode selected, goes from 00 to 04. Mouse will take this into account when toggling DPI again.
11 - trippy bit. If set to 00 the mouse will slow down in time... no really, the refresh rate will feel extremely sluggish and the mouse will keep moving for seconds after you let go. Anything else than 00 makes it work ok.
22 - Seems to be always 04, if you change it to something else and start switching DPI the mouse will become faster than at the maximum default or can even completely stop.
!!!! - DPI to be set. It doesn't actually use the last 2 bytes so only first 2 are needed.
```

Base value for 100Hz is 0101 (or 01 if you ignore the last 2 bytes), then the next 100Hz increments add - 03, 02, 02 and then start again from 03.


Here's a python program that computes the insanity they used to select the DPI.

```
>>> def gen_num(n):
...     x = 0
...     for i in range(n):
...         if i %3 == 1:
...             x += 1
...     y = n - x - 1
...     print(x,y,n)
...     return 0x0101 + 0x0303*x + 0x0202*y
...
>>> hex(gen_num(30))
10 19 30
'0x4545'
```
#### In-memory
- [ ] Reversed
- [ ] Implemented

### Get DPI

#### Current
Getting the initial value upon driver startup does not seem possible. The proprietary driver "gets" initial DPI by simply setting it itself to the last value it known.

Getting subsequent DPI seems possible but the driver seems to set the mouse to some mode where it reports DPI upon setting it, other than that the mouse does not communicate DPI change.

#### In-memory
- [ ] Reversed
- [ ] Implemented

```
0702002a006000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
```

Still have yet to understand 8 bytes in the response, so not fully reversed yet. For partial example look in data.txt

## Backlight intensity

### Set & Get

#### Current
- [X] Implemented

```
07110000800000000300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
       11       22
11 - get/set bit. 00 means get, 08 means set.
22 - can be anywhere from 1 to 3, anything else means off. 3 is most intense.
```

#### In-memory
- [ ] Reversed
- [ ] Implemented

No idea.

## RGB control

The driver allows to set animation profiles in memory that can then be switched by physically lifting the mouse and pressing the extra button marked "1".

It also has an RGB editor that lets you test stuff out.

### Edit mode

Entering and quitting edit mode
```
07030600000000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
07030600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
                ##
## - set to 01 for edit mode(can control individual LEDs through commands) or to 00 to have the mouse profile(s) running instead
```
Lighting up LEDs with colors you want in edit mode.

```
0703060200000000ff0000000000ff00ff7fff007f00ff00ff0000ffff007fff0000000000000000000000000000000000000000000000000000000000000000
0703060200000000ffffffffffffffffffffffffffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000
                111111222222333333444444555555666666777777888888
                one led has ff:ff:ff each of which controls R:G:B intensity
```
